using System;
using System.Collections.Generic;
using DefaultNamespace.ViewModel;
using TMPro;
using UnityEngine;

namespace DefaultNamespace.UI
{
    public class ShopUIView : MonoBehaviour
    {
        public ShopViewModel viewModel;
        public ShopButtonView shopButtonPrefab;

        public GameObject shopButtonsParent;
        public TextMeshProUGUI weaponLabel;
        public TextMeshProUGUI coinsCounter;

        private bool isInitialized;

        private void OnEnable()
        {
            viewModel.shopButtonStates.OnValueChanged += InitializeIfNeed;
            viewModel.previewedWeaponLabel.OnValueChanged += OnPreviewedWeaponChanged;
            viewModel.currencyCounter.OnValueChanged += OnCoinsAmountChanged;
        }

        private void OnCoinsAmountChanged(int coinsAmount)
        {
            coinsCounter.text = coinsAmount.ToString();
        }

        private void OnDisable()
        {
            viewModel.shopButtonStates.OnValueChanged -= InitializeIfNeed;
            viewModel.previewedWeaponLabel.OnValueChanged -= OnPreviewedWeaponChanged;
        }

        private void InitializeIfNeed(List<ShopButtonState> buttonStates)
        {
            if (isInitialized)
            {
                return;
            }
            
            for (var i = 0; i < buttonStates.Count; i++)
            {
                var shopButtonView = Instantiate(shopButtonPrefab, shopButtonsParent.transform);
                shopButtonView.Initialize(buttonStates[i].id);
            }

            isInitialized = true;
        }

        private void OnPreviewedWeaponChanged(string weaponName)
        {
            weaponLabel.text = weaponName;
        }
    }
}