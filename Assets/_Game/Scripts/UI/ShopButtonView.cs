using System;
using System.Collections.Generic;
using DefaultNamespace.ViewModel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class ShopButtonView : MonoBehaviour
    {
        public ShopViewModel viewModel;
        
        public Button purchaseButton;
        public Button previewButton;
        public Image previewIcon;
        public GameObject purchaseIndicator;
        public TextMeshProUGUI priceLabel;

        private string id;

        private void OnEnable()
        {
            purchaseButton.onClick.AddListener(PurchaseWeaponButtonClick);
            previewButton.onClick.AddListener(PreviewWeaponButtonClick);
            viewModel.shopButtonStates.OnValueChanged += HandleShopButtonStates;
        }

        private void OnDisable()
        {
            purchaseButton.onClick.RemoveListener(PurchaseWeaponButtonClick);
            previewButton.onClick.RemoveListener(PreviewWeaponButtonClick);
        }

        public void Initialize(string id)
        {
            this.id = id;
            HandleShopButtonStates(viewModel.shopButtonStates.Value);
        }

        private void HandleShopButtonStates(List<ShopButtonState> states)
        {
            for (var i = 0; i < states.Count; i++)
            {
                var state = states[i];
                if (state.id == id)
                {
                    SetState(state);
                    return;
                }
            }
        }

        private void SetState(ShopButtonState state)
        {
            previewIcon.sprite = state.icon;
            priceLabel.text = state.price.ToString();
            purchaseButton.gameObject.SetActive(!state.isPurchased);
            purchaseIndicator.SetActive(state.isPurchased);
        }

        private void PurchaseWeaponButtonClick() => viewModel.PurchaseWeaponButtonClick(id);
        private void PreviewWeaponButtonClick() => viewModel.PreviewWeaponButtonClick(id);
    }

    public readonly struct ShopButtonState
    {
        public readonly string id;
        public readonly Sprite icon;
        public readonly int price;
        public readonly bool isPurchased;

        public ShopButtonState(string id, Sprite icon, int price, bool isPurchased)
        {
            this.id = id;
            this.icon = icon;
            this.price = price;
            this.isPurchased = isPurchased;
        }
    }
}