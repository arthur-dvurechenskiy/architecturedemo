using System;
using UnityEngine;

namespace DefaultNamespace.Manager
{
    [CreateAssetMenu(menuName = "_Game/Manager/LocalizationManager")]
    public class LocalizationManager : ScriptableObject
    {
        // Very simple implementation of localization mechanism. In real world, you would probably
        // fetch data from server, but for simplicity of this example, values just hardcoded
        public string GetLocalizedValue(WeaponType key)
        {
            var isLanguageRussian = Application.systemLanguage == SystemLanguage.Russian;
            
            switch (key)
            {
                case WeaponType.Sword:
                    return isLanguageRussian ? "Меч" : "Sword";
                case WeaponType.Mace:
                    return isLanguageRussian ? "Дубина" : "Mace";
                case WeaponType.Axe:
                    return isLanguageRussian ? "Топор" : "Axe";
                default:
                    throw new ArgumentOutOfRangeException(nameof(key), key, null);
            }
        }
    }
}