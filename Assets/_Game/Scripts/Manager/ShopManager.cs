using System.Collections.Generic;
using System.Linq;
using DefaultNamespace.ViewModel;
using UnityEngine;

namespace DefaultNamespace.Manager
{
    public class ShopManager : MonoBehaviour
    {
        public ShopViewModel viewModel;
        public Storage storage;
        public LocalizationManager localizationManager;
        public ABTestManager abTestManager;
        public List<Weapon> weapons;

        private GameObject weaponInstance;

        private void Start()
        {
            UpdateShopButtonsStates();
            // by default preview first weapon in the list
            OnWeaponPreviewButtonClicked(weapons[0].id);
            viewModel.currencyCounter.Value = storage.Coins;
        }

        private void OnEnable()
        {
            viewModel.OnWeaponPreviewButtonClicked += OnWeaponPreviewButtonClicked;
            viewModel.OnWeaponPurchaseButtonClicked += OnWeaponPurchaseButtonClicked;
        }

        private void OnDisable()
        {
            viewModel.OnWeaponPreviewButtonClicked -= OnWeaponPreviewButtonClicked;
            viewModel.OnWeaponPurchaseButtonClicked -= OnWeaponPurchaseButtonClicked;
        }

        private void OnWeaponPurchaseButtonClicked(string weaponId)
        {
            var weapon = GetWeaponById(weaponId);
            var weaponPrice = GetWeaponPrice(weapon);
            if (weaponPrice <= storage.Coins)
            {
                storage.Coins -= weaponPrice;
                viewModel.currencyCounter.Value = storage.Coins;
                storage.PurchaseWeapon(weapon.id);
                OnWeaponPreviewButtonClicked(weaponId);
                UpdateShopButtonsStates();
            }
            else
            {
                Debug.Log("Don't have enough coins to purchase weapon");
            }
        }

        private void OnWeaponPreviewButtonClicked(string weaponId)
        {
            var weapon = GetWeaponById(weaponId);
            if (weaponInstance)
            {
                Destroy(weaponInstance);
            }
            weaponInstance = Instantiate(weapon.prefab, transform);
            viewModel.previewedWeaponLabel.Value = localizationManager.GetLocalizedValue(weapon.type);
        }

        private void UpdateShopButtonsStates()
        {
            viewModel.shopButtonStates.Value = weapons
                .Select(weapon => new ShopButtonState(
                    weapon.id,
                    weapon.icon,
                    GetWeaponPrice(weapon),
                    storage.IsWeaponPurchased(weapon.id)
                ))
                .ToList();
        }
        
        private Weapon GetWeaponById(string weaponId) => weapons.First(weapon => weapon.id == weaponId);

        private int GetWeaponPrice(Weapon weapon) => (int) (abTestManager.priceCoefficient * weapon.price);
    }
}