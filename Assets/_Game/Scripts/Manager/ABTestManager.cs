using UnityEngine;

namespace DefaultNamespace.Manager
{
    [CreateAssetMenu(menuName = "_Game/Manager/ABTestManager")]
    public class ABTestManager : ScriptableObject
    {
        // Very simple implementation of AB tests mechanism. In real world, you would fetch data
        // from server, but for simplicity of this example, implemented as field
        public float priceCoefficient;
    }
}