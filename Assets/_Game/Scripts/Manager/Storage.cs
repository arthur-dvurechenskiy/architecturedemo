using System;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace.Manager
{
    /// <summary>
    /// Data storage mechanism. For simplicity of this example, all data is stored in memory,
    /// not in persistent storage and reset on play mode exit. 
    /// </summary>
    [CreateAssetMenu(menuName = "_Game/Manager/Storage")]
    public class Storage : ScriptableObject
    {
        private int coins = 50;
        private List<string> purchasedWeapons = new List<string>();

        public int Coins
        {
            get => coins;
            set => coins = value;
        }

        public bool IsWeaponPurchased(string weaponId) => purchasedWeapons.Contains(weaponId);

        public void PurchaseWeapon(string weaponId)
        {
            purchasedWeapons.Add(weaponId);
        }

        private void OnDisable()
        {
            coins = 50;
            purchasedWeapons.Clear();
        }
    }
}