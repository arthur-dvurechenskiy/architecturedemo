using System;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace.ViewModel
{
    [CreateAssetMenu(menuName = "_Game/ViewModel/ShopViewModel")]
    public class ShopViewModel: ScriptableObject
    {
        public event Action<string> OnWeaponPreviewButtonClicked = weaponId => { };
        public event Action<string> OnWeaponPurchaseButtonClicked = weaponId => { };

        public ObservableVariable<int> currencyCounter = new ObservableVariable<int>(0);
        public ObservableVariable<string> previewedWeaponLabel = new ObservableVariable<string>(null);
        public ObservableVariable<List<ShopButtonState>> shopButtonStates = new ObservableVariable<List<ShopButtonState>>(null);

        public void PreviewWeaponButtonClick(string weaponId)
        {
            OnWeaponPreviewButtonClicked.Invoke(weaponId);
        }

        public void PurchaseWeaponButtonClick(string weaponId)
        {
            OnWeaponPurchaseButtonClicked.Invoke(weaponId);
        }
    }
}