using System;
using UnityEngine;

namespace DefaultNamespace
{
    /// <summary>
    /// We could use models (SO) in views directly, but usually we want to handle raw data somehow,
    /// before display it.  
    /// </summary>
    [CreateAssetMenu(menuName = "_Game/Weapon")]
    public class Weapon : ScriptableObject
    {
        public string id = Guid.NewGuid().ToString();
        public Sprite icon;
        public int price;
        public WeaponType type;
        public GameObject prefab;
    }

    public enum WeaponType
    {
        Sword,
        Mace,
        Axe,
    }
}