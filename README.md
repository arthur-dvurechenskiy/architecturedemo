This project contains MVVM architecture described here https://app.clickup.com/24424206/v/dc/q9bre-14701/q9bre-5481

Project contains a simple Weapon Shop scene. Player can select one of several weapons, preview it and buy.

Project contains example of mechanisms, which are used in real-world projects: AB-tests, localization, data storage. Implementation of these mechanisms is very small and rather naive, as the main purpose of this project is to demonstrate, how them can be wired together in order to interact with UI in a simple and unified way. 
